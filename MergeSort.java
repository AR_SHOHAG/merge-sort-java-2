import java.util.Scanner;

public class MergeSort {

	public void merge_sort(int A[], int p, int r)
	{
		int q;

		if(p<r){
			q = (p+r)/2; // floor val of q
			merge_sort(A, p, q);
			merge_sort(A, q+1, r);
			Merge(A, p, q, r);
		}
	}

	public void Merge(int A[], int p, int q, int r)
	{
		int n1, n2, i, j, k;
		double inf = Double.POSITIVE_INFINITY;

		n1 = q-p+1;
		n2 = r-q;

		int L[] = new int[n1+1];
		int R[] = new int[n2+1];

		for(i=0; i<n1; ++i){
			L[i] = A[p+i];
		}

		for(j=0; j<n2; ++j){
			R[j] = A[q+j+1];
		}

		L[n1]= (int)inf;
		R[n2]= (int)inf;
		i=0;
		j=0;

		for(k=p; k<=r; ++k){
			if(L[i]<=R[j]){
				A[k]=L[i];
				i++;
			}
			else{
				A[k]=R[j];
				j++;
			}
		}

	}

	public static void main(String[] args) {

		int size;

		Scanner in = new Scanner(System.in);

		System.out.print("Please Enter The Size of Array: ");
		size = in.nextInt();

		int A[] = new int[size];

		System.out.print("Enter " + size + " integers: ");
		for(int l =0; l<size; ++l){
			A[l] = in.nextInt();
		}

		System.out.print("Original Array: ");
		for(int m =0; m<size; ++m){
			System.out.print(A[m] + "  ");
		}

		int p, r;
		p = 0;
		r = A.length -1;

		MergeSort M = new MergeSort();
		M.merge_sort(A, p, r);

		System.out.print("\nSorted Array: ");
		for(int n=0; n<A.length; ++n){
            System.out.print(A[n] + "  ");
		}
        System.out.println();

	}

}
